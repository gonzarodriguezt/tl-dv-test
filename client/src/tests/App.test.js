import { render, screen } from "@testing-library/react";
import App from "../App";

test("Renders correctly", () => {
  render(<App />);
  const homeElement = screen.getByText(/Home/i);
  expect(homeElement).toBeInTheDocument();
});
