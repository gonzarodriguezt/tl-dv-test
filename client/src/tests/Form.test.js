import { render, screen } from "@testing-library/react";
import { Form } from "../components/Form";

test("Form renders state", async () => {
  render(<Form state={{ slug: "slug", title: "title", url: "url" }} />);

  const slugInput = screen.getByTestId("slug");
  expect(slugInput.value).toBe("slug");

  const titleInput = screen.getByTestId("title");
  expect(titleInput.value).toBe("title");

  const urlInput = screen.getByTestId("url");
  expect(urlInput.value).toBe("url");
});
