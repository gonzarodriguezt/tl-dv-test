const host = "http://localhost:1337";

export const get = async (slug) => {
  const response = await fetch(`${host}/${slug}`);
  return response.json();
};

export const put = async (slug, data) => {
  const response = await fetch(`${host}/${slug}`, {
    method: "PUT",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
    },
  });
  return response.json();
};

export const post = async (slug, data) => {
  const response = await fetch(`${host}/${slug}`, {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
    },
  });
  return response.json();
};
