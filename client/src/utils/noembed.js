export const getNoembedData = async (url) => {
  const data = await fetch(`http://noembed.com/embed?url=${url}`);
  return await data.json();
};
