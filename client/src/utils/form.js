export const parseFormData = (data) => ({
  ...data,
  isPublic: !!parseInt(data.isPublic),
});
