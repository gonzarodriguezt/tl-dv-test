import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { Container, Form, Menu, StyledLink } from "../components";
import { put, get } from "../utils/api";
import { parseFormData } from "../utils/form";

const EditVideo = () => {
  const { slug } = useParams();
  const history = useHistory();
  const [state, setState] = useState();

  useEffect(() => {
    (async () => {
      const [data] = await get(`videos?slug=${slug}`);
      setState(data);
    })();
  }, []);

  const onSubmit = async (data) => {
    const res = await put(`videos/${state.id}`, {
      ...state,
      ...parseFormData(data),
    });
    setState(res);
    history.push(`/videos/${data.slug}`);
  };

  return (
    <Container>
      <Menu>
        <StyledLink to={`/`}>Home</StyledLink>
        <StyledLink to={{ pathname: `/videos/${state?.slug}`, state }}>
          Back
        </StyledLink>
      </Menu>
      <Form state={state} onSubmit={onSubmit} />
    </Container>
  );
};

export default EditVideo;
