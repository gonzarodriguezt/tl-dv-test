import React from "react";
import { useHistory } from "react-router-dom";
import { Container, Menu, Form, StyledLink } from "../components";
import { post } from "../utils/api";
import { parseFormData } from "../utils/form";

const AddVideo = () => {
  const history = useHistory();

  const onSubmit = async (data) => {
    if (!data.url) return;
    const res = await post(`videos/`, parseFormData(data));
    history.push(`/videos/${res.slug}`);
  };

  return (
    <Container>
      <Menu>
        <StyledLink to="/">Home</StyledLink>
      </Menu>
      <Form onSubmit={onSubmit} />
    </Container>
  );
};
export default AddVideo;
