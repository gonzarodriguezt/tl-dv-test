import React, { useEffect, useState } from "react";
import ReactPlayer from "react-player";
import { useParams } from "react-router-dom";
import { Container, Menu, PlayerWrapper, StyledLink } from "../components";
import { Title } from "../components/Video/elements";
import { get } from "../utils/api";

const Player = () => {
  const { slug } = useParams();
  const [state, setState] = useState();

  useEffect(() => {
    (async () => {
      const [data] = await get(`videos?slug=${slug}`);
      setState(data);
    })();
  }, []);

  return (
    <Container>
      <Menu>
        <StyledLink to="/">Home</StyledLink>
        <StyledLink to={{ pathname: `/videos/${slug}/edit`, state }}>
          Edit
        </StyledLink>
      </Menu>
      {!state && <p>Loading...</p>}
      {state && (
        <>
          <Title>{state.title}</Title>
          <PlayerWrapper>
            <ReactPlayer
              url={state.url}
              width="100%"
              height="100%"
              style={{ position: "absolute", top: 0, left: 0 }}
              playing={true}
            />
          </PlayerWrapper>
        </>
      )}
    </Container>
  );
};

export default Player;
