import React, { useEffect, useState } from "react";
import {
  Container,
  Menu,
  StyledLink,
  Video,
  VideosContainer,
} from "../components";
import { get } from "../utils/api";
import { useHistory } from "react-router-dom";

const Videos = () => {
  const [videos, setVideos] = useState();
  const history = useHistory();

  useEffect(() => {
    (async () => {
      const data = await get("videos");
      setVideos(data);
    })();
  }, []);

  const onClickVideo = ({ url, slug, title, id }) =>
    history.push(`/videos/${slug}`, { url, title, slug, id });

  const isEmpty = videos && videos?.length <= 0;
  const isAllPrivate =
    !isEmpty && Array.isArray(videos) && !videos?.some((v) => v.isPublic);

  return (
    <Container>
      <Menu>
        <StyledLink to="/">Home</StyledLink>
        <StyledLink to="add">Add new</StyledLink>
      </Menu>
      {videos?.statusCode === 403 && (
        <h2>Please check your user permissions</h2>
      )}
      {Array.isArray(videos) && (
        <VideosContainer>
          {(isEmpty || isAllPrivate) && <h2>No videos to show yet</h2>}
          {!isEmpty &&
            videos
              ?.map(
                ({ title, slug, url, id, isPublic }) =>
                  isPublic && (
                    <Video
                      onClick={() => onClickVideo({ url, slug, title, id })}
                      title={title}
                      slug={slug}
                      url={url}
                    />
                  )
              )}
        </VideosContainer>
      )}
    </Container>
  );
};

export default Videos;
