import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AddVideo from "./pages/AddVideo";
import EditVideo from "./pages/EditVideo";
import Player from "./pages/Player";
import Videos from "./pages/Videos";

const App = () => (
  <>
    <Router>
      <Switch>
        <Route path="/add" component={AddVideo} />
        <Route path="/videos/:slug/edit" component={EditVideo} />
        <Route path="/videos/:slug" component={Player} />
        <Route path="/" component={Videos} />
      </Switch>
    </Router>
  </>
);

export default App;
