import styled from "styled-components";

export const Menu = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;
