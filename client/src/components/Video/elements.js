import styled from "styled-components";

export const Image = styled.img`
  width: 100%;
  height: 100%;
  position: absolute;
  object-fit: cover;
`;

export const ImageContainer = styled.div`
  width: 100%;
  padding-bottom: 56.25%;
  position: relative;
`;

export const Card = styled.div`
  height: 100%;
  margin-top: 1rem;
  display: flex;
  flex-direction: column;
  cursor: pointer;

  &:nth-child(even) {
    padding-left: 0.5rem;
  }
  &:nth-child(odd) {
    padding-right: 0.5rem;
  }

  @media (max-width: 768px) {
    &:nth-child(even) {
      padding-left: 0rem;
    }
    &:nth-child(odd) {
      padding-right: 0rem;
    }
  }
`;

export const Title = styled.h2`
  color: #000;
`;

export const Placeholder = styled.div`
  background-color: #e8e8e8;
  position: absolute;
  width: 100%;
  height: 100%;
`;
