import { useEffect, useState } from "react";
import { getNoembedData } from "../../utils/noembed";
import { Card, Image, ImageContainer, Placeholder, Title } from "./elements";

export const Video = ({ title, slug, url, onClick }) => {
  const [img, setImg] = useState();

  useEffect(() => {
    (async () => {
      const data = await getNoembedData(url);
      setImg(data?.thumbnail_url);
    })();
  }, [url]);

  return (
    <Card onClick={onClick}>
      <ImageContainer>
        {!img && <Placeholder />}
        {img && <Image src={img} alt={slug} />}
      </ImageContainer>
      <Title>{title}</Title>
    </Card>
  );
};
