import styled from "styled-components";

export const Container = styled.div`
  background-color: #fff;
  margin: auto;
  max-width: 50%;
  padding: 2rem 0;

  @media (max-width: 768px) {
    max-width: 80%;
  }
`;
