import styled from "styled-components";
import { Link } from "react-router-dom";

export const StyledLink = styled(Link)`
  color: blue;
  font-weight: 600;
  text-decoration: none;
`;
