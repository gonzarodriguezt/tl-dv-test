import React from "react";
import { useForm } from "react-hook-form";
import {
  Button,
  Form as StyledForm,
  Input,
  Label,
  Radio,
  Radios,
} from "./elements";

export const Form = ({ state, onSubmit }) => {
  const { register, handleSubmit } = useForm();

  return (
    <StyledForm>
      <Input
        name="slug"
        ref={register}
        placeholder="Write a slug"
        defaultValue={state?.slug}
        data-testid="slug"
      />
      <Input
        name="title"
        ref={register}
        placeholder="Choose a title"
        defaultValue={state?.title}
        data-testid="title"
      />
      <Input
        name="url"
        ref={register}
        placeholder="Paste the url"
        defaultValue={state?.url}
        data-testid="url"
      />
      <Radios>
        <Radio
          ref={register}
          type="radio"
          name="isPublic"
          value={1}
          defaultChecked={state ? state?.isPublic : true}
        />
        <Label htmlFor="isPublic">Public</Label>
        <Radio
          ref={register}
          type="radio"
          name="isPublic"
          value={0}
          defaultChecked={state ? !state?.isPublic : false}
        />
        <Label htmlFor="isPublic">Private</Label>
      </Radios>
      <Button onClick={handleSubmit(onSubmit)} data-testid="button">
        Save
      </Button>
    </StyledForm>
  );
};
