import styled from "styled-components";

export const Form = styled.form`
  width: 100%;
  margin: 1rem 0;
  display: flex;
  flex-direction: column;

  @media (max-width: 768px) {
    width: 100%;
  }
`;

export const Input = styled.input`
  margin-bottom: 0.5rem;
  height: 30px;
  padding: 0 0.5rem;
  border-radius: 5px;
  border: 1px solid #cdcdcd;
`;

export const Button = styled.button`
  height: 35px;
  padding: 0 0.5rem;
  background-color: blue;
  border: none;
  color: white;
  border-radius: 5px;
  font-weight: 500;
`;

export const Radios = styled.div`
  margin-bottom: 0.5rem;
  display: flex;
  align-items: center;
`;

export const Radio = styled.input`
  margin: 0.5rem;
  margin-right: 0;
`;

export const Label = styled.label`
  color: #000;
  font-size: 0.8rem;
  margin-left: 0.5rem;
`;
