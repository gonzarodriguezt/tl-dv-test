import styled from "styled-components";

export const PlayerWrapper = styled.div`
  padding-top: 56.25%;
  position: relative;
`;
