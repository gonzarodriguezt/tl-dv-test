# tl;dv - Test

The technologies I used are Javascript, Strapi as the main framework for the API. MongoDB for the DB and react-testing-library with Jest for testing.

This project implements all the bullet points on the Notion page. Also, on the `frontend-bonus` branch, the bonus points are available.

### Functionality

On the `frontend-bonus` branch, a new video can be created and the app will redirect to the page of that video where an `Edit` button will be available.

If `public` was selected at creation, it will be shown on the Home page with the corresponding thumbnail if the host used to share the video is supported.

The app is also responsive and lightweight made with `styled-components`.

Minimal tests were also written.

---

## Bootstrap

For this application to work, Node and MongoDB are needed. The app uses Yark workspaces.

To install dependencies:

```
yarn
```

With MongoDB started, run:

```
 yarn workspace @tl-dv-test/api start
```

Before running the frontend, we need to go to [user permissions](http://localhost:1337/admin/settings/users-permissions/roles), and allow public permissions for all fields.

Then, to start the frontend:

```
 yarn workspace @tl-dv-test/client start
```

---

## Tests

Run with

```
 yarn workspace @tl-dv-test/client test
```
